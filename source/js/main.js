'use strict';
//= ../node_modules/lazysizes/lazysizes.js

//= ../node_modules/jquery/dist/jquery.js

//= ../node_modules/popper.js/dist/umd/popper.js

//= ../node_modules/bootstrap/js/dist/util.js
//= ../node_modules/bootstrap/js/dist/alert.js
//= ../node_modules/bootstrap/js/dist/button.js
//= ../node_modules/bootstrap/js/dist/carousel.js
//= ../node_modules/bootstrap/js/dist/collapse.js
//= ../node_modules/bootstrap/js/dist/dropdown.js
//= ../node_modules/bootstrap/js/dist/modal.js
//= ../node_modules/bootstrap/js/dist/tooltip.js
//= ../node_modules/bootstrap/js/dist/popover.js
//= ../node_modules/bootstrap/js/dist/scrollspy.js
//= ../node_modules/bootstrap/js/dist/tab.js
//= ../node_modules/bootstrap/js/dist/toast.js

//= ../node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js
//= library/wow.js
//= library/slick.js
//= library/jquery-ui.js



$(document).ready(function () {



    function ratePrice () {
        var inputValue      = $("#slider").slider('value');
        var precent         = $("input[name=nalog]:checked").val();
        var type            = $(".sort select").val();


        var length = rate.length;

        // перебрать весь массив
        for (var i = 0; i < length; i++) {
            // определить вид деятельности
            if ( rate[i].field1 == type ) {

                // определить процент
                if ( parseInt( rate[i].field2 ) == precent) {

                    // определить диапозон
                    if ( parseInt( inputValue ) < parseInt( rate[i].field3 ) ) {

                        // нашли нужную строку

                        // вставить в таблицу
                        // первый тариф
                        if ( rate[i].field4 == "" ) {
                            // отключить тариф
                            toggleRate( ".calc__v_1", rate[i].field4, 1, false );
                        } else {
                            // ключить тариф + добавить значение
                            toggleRate( ".calc__v_1", rate[i].field4, 1, true );
                        }

                        // Второй тариф
                        if ( rate[i].field5 ==  "") {
                            // отключить тариф
                            toggleRate( ".calc__v_2", rate[i].field5, 2, false );
                        } else {
                            // ключить тариф + добавить значение
                            toggleRate( ".calc__v_2", rate[i].field5, 2, true );
                        }

                        // Третий тариф
                        if ( rate[i].field6 ==  "") {
                            // отключить тариф
                            toggleRate( ".calc__v_3", rate[i].field6, 3, false );
                        } else {
                            // ключить тариф + добавить значение
                            toggleRate( ".calc__v_3", rate[i].field6, 3, true );
                        }

                        // Четвертый тариф
                        if ( rate[i].field7 ==  "") {
                            // отключить тариф
                            toggleRate( ".calc__v_4", rate[i].field7, 4, false );
                        } else {
                            // ключить тариф + добавить значение
                            toggleRate( ".calc__v_4", rate[i].field7, 4, true );
                        }


                        // Шестой тариф
                        if ( rate[i].field8 ==  "") {
                            // отключить тариф
                            toggleRate( ".calc__v_5", rate[i].field8, 5, false );
                        } else {
                            // ключить тариф + добавить значение
                            toggleRate( ".calc__v_5", rate[i].field8, 5, true );
                        }

                        break;
                    }
                }
            }
        }
    }

    function toggleRate(element, value, index, active) {
        if( active )
            $(element).html(value + " ₽");
        else 
            $(element).html("disable");

        if ( active )
            $(element).parent().removeClass("calc_disable");
        else
            $(element).parent().addClass("calc_disable");
        
        // Отключаем строки с картинками
        var completeRow = $(".calc_h_a");
        var length = completeRow.length;
        for ( var i = 0; i < length ; i++) {
            var deepComplete = $(completeRow[i]).find(".calc_h");
            var lengthDeep = deepComplete.length;

            for (var j = 0; j < lengthDeep; j++) {
                var btn = $(".calc_h_a").find(".calc__btn");
                var btnMobile = $(".btn__m_calc");

                if ( active ) {
                    $( deepComplete[index] ).removeClass("calc_disable");
                    $( btn[index] ).removeAttr("disabled");
                    $( btnMobile[index] ).removeAttr("disabled");
                }
                else {
                    $( deepComplete[index] ).addClass("calc_disable");
                    $( btn[index] ).attr("disabled","disabled");
                    $( btnMobile[index] ).attr("disabled","disabled");
                }

                break;
            }
                
        }

        
    }

    function disableRate(element, value, index) {
        $(element).html(value + " ₽");
        $(element).parent().addClass("calc_disable");

        var completeRow = $(".calc_h_a");
        var length = completeRow.length;
        for ( var i = 0; i < length; i++) {
            var deepComplete = $(completeRow[i]).find(".calc_h");
            var length = deepComplete.length;
            for (var j = 0; j < length; j++) {
                $( deepComplete[index] ).addClass("calc_disable");
                break;
            }
        }
    }


    var rate = [
                {"field1":"v1","field2":"0","field3":"99999",       "field4":"10000",   "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v1","field2":"0","field3":"249999",      "field4":"10000",   "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v1","field2":"0","field3":"499999",      "field4":"10000",   "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v1","field2":"0","field3":"999999",      "field4":"15000",   "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v1","field2":"0","field3":"1999999",     "field4":"22500",   "field5":"",        "field6":"",        "field7":"25000",   "field8":"40000"},
                {"field1":"v1","field2":"0","field3":"2999999",     "field4":"25000",   "field5":"",        "field6":"",        "field7":"30000",   "field8":"45000"},
                {"field1":"v1","field2":"0","field3":"3999999",     "field4":"25000",   "field5":"",        "field6":"",        "field7":"35000",   "field8":"50000"},
                {"field1":"v1","field2":"0","field3":"4999999",     "field4":"25000",   "field5":"",        "field6":"",        "field7":"40000",   "field8":"55000"},
                {"field1":"v1","field2":"0","field3":"5999999",     "field4":"",        "field5":"",        "field6":"30000",   "field7":"45000",   "field8":"60000"},
                {"field1":"v1","field2":"0","field3":"6999999",     "field4":"",        "field5":"",        "field6":"32000",   "field7":"50000",   "field8":"65000"},
                {"field1":"v1","field2":"0","field3":"7999999",     "field4":"",        "field5":"",        "field6":"34000",   "field7":"55000",   "field8":"70000"},
                {"field1":"v1","field2":"0","field3":"8999999",     "field4":"",        "field5":"",        "field6":"36000",   "field7":"60000",   "field8":"75000"},
                {"field1":"v1","field2":"0","field3":"9999999",     "field4":"",        "field5":"",        "field6":"38000",   "field7":"65000",   "field8":"80000"},
                {"field1":"v1","field2":"0","field3":"10999999",    "field4":"",        "field5":"",        "field6":"40000",   "field7":"70000",   "field8":"85000"},
                {"field1":"v1","field2":"0","field3":"11999999",    "field4":"",        "field5":"",        "field6":"42000",   "field7":"75000",   "field8":"90000"},
                {"field1":"v1","field2":"0","field3":"12999999",    "field4":"",        "field5":"",        "field6":"44000",   "field7":"80000",   "field8":"95000"},
                {"field1":"v1","field2":"0","field3":"13999999",    "field4":"",        "field5":"",        "field6":"46000",   "field7":"85000",   "field8":"100000"},
                {"field1":"v1","field2":"0","field3":"14999999",    "field4":"",        "field5":"",        "field6":"48000",   "field7":"90000",   "field8":"105000"},
                {"field1":"v1","field2":"0","field3":"15999999",    "field4":"",        "field5":"",        "field6":"50000",   "field7":"95000",   "field8":"110000"},
                {"field1":"v1","field2":"0","field3":"16999999",    "field4":"",        "field5":"",        "field6":"52000",   "field7":"100000",  "field8":"115000"},
                {"field1":"v1","field2":"0","field3":"17999999",    "field4":"",        "field5":"",        "field6":"54000",   "field7":"105000",  "field8":"120000"},
                {"field1":"v1","field2":"0","field3":"18999999",    "field4":"",        "field5":"",        "field6":"56000",   "field7":"110000",  "field8":"125000"},
                {"field1":"v1","field2":"0","field3":"20000000",    "field4":"",        "field5":"",        "field6":"58000",   "field7":"115000",  "field8":"130000"},
    
                {"field1":"v1","field2":"15","field3":"99999",      "field4":"10000",   "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v1","field2":"15","field3":"249999",     "field4":"10000",   "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v1","field2":"15","field3":"499999",     "field4":"10000",   "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v1","field2":"15","field3":"999999",     "field4":"15000",   "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v1","field2":"15","field3":"1999999",    "field4":"22500",   "field5":"",        "field6":"",        "field7":"25000",   "field8":"40000"},
                {"field1":"v1","field2":"15","field3":"2999999",    "field4":"25000",   "field5":"",        "field6":"",        "field7":"30000",   "field8":"45000"},
                {"field1":"v1","field2":"15","field3":"3999999",    "field4":"25000",   "field5":"",        "field6":"",        "field7":"35000",   "field8":"50000"},
                {"field1":"v1","field2":"15","field3":"4999999",    "field4":"25000",   "field5":"",        "field6":"",        "field7":"40000",   "field8":"55000"},
                {"field1":"v1","field2":"15","field3":"5999999",    "field4":"",        "field5":"",        "field6":"30000",   "field7":"45000",   "field8":"60000"},
                {"field1":"v1","field2":"15","field3":"6999999",    "field4":"",        "field5":"",        "field6":"32000",   "field7":"50000",   "field8":"65000"},
                {"field1":"v1","field2":"15","field3":"7999999",    "field4":"",        "field5":"",        "field6":"34000",   "field7":"55000",   "field8":"70000"},
                {"field1":"v1","field2":"15","field3":"8999999",    "field4":"",        "field5":"",        "field6":"36000",   "field7":"60000",   "field8":"75000"},
                {"field1":"v1","field2":"15","field3":"9999999",    "field4":"",        "field5":"",        "field6":"38000",   "field7":"65000",   "field8":"80000"},
                {"field1":"v1","field2":"15","field3":"10999999",   "field4":"",        "field5":"",        "field6":"40000",   "field7":"70000",   "field8":"85000"},
                {"field1":"v1","field2":"15","field3":"11999999",   "field4":"",        "field5":"",        "field6":"42000",   "field7":"75000",   "field8":"90000"},
                {"field1":"v1","field2":"15","field3":"12999999",   "field4":"",        "field5":"",        "field6":"44000",   "field7":"80000",   "field8":"95000"},
                {"field1":"v1","field2":"15","field3":"13999999",   "field4":"",        "field5":"",        "field6":"46000",   "field7":"85000",   "field8":"100000"},
                {"field1":"v1","field2":"15","field3":"14999999",   "field4":"",        "field5":"",        "field6":"48000",   "field7":"90000",   "field8":"105000"},
                {"field1":"v1","field2":"15","field3":"15999999",   "field4":"",        "field5":"",        "field6":"50000",   "field7":"95000",   "field8":"110000"},
                {"field1":"v1","field2":"15","field3":"16999999",   "field4":"",        "field5":"",        "field6":"52000",   "field7":"100000",  "field8":"115000"},
                {"field1":"v1","field2":"15","field3":"17999999",   "field4":"",        "field5":"",        "field6":"54000",   "field7":"105000",  "field8":"120000"},
                {"field1":"v1","field2":"15","field3":"18999999",   "field4":"",        "field5":"",        "field6":"56000",   "field7":"110000",  "field8":"125000"},
                {"field1":"v1","field2":"15","field3":"20000000",   "field4":"",        "field5":"",        "field6":"58000",   "field7":"115000",  "field8":"130000"},

                {"field1":"v1","field2":"6","field3":"99999",       "field4":"2500",    "field5":"10000",   "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v1","field2":"6","field3":"249999",      "field4":"4200",    "field5":"10000",   "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v1","field2":"6","field3":"499999",      "field4":"6500",    "field5":"10000",   "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v1","field2":"6","field3":"999999",      "field4":"",        "field5":"10000",   "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v1","field2":"6","field3":"1999999",     "field4":"",        "field5":"12000",   "field6":"",        "field7":"25000",   "field8":"40000"},
                {"field1":"v1","field2":"6","field3":"2999999",     "field4":"",        "field5":"14000",   "field6":"",        "field7":"30000",   "field8":"45000"},
                {"field1":"v1","field2":"6","field3":"3999999",     "field4":"",        "field5":"16000",   "field6":"",        "field7":"35000",   "field8":"50000"},
                {"field1":"v1","field2":"6","field3":"4999999",     "field4":"",        "field5":"18000",   "field6":"",        "field7":"40000",   "field8":"55000"},
                {"field1":"v1","field2":"6","field3":"5999999",     "field4":"",        "field5":"20000",   "field6":"",        "field7":"45000",   "field8":"60000"},
                {"field1":"v1","field2":"6","field3":"6999999",     "field4":"",        "field5":"22000",   "field6":"",        "field7":"50000",   "field8":"65000"},
                {"field1":"v1","field2":"6","field3":"7999999",     "field4":"",        "field5":"24000",   "field6":"",        "field7":"55000",   "field8":"70000"},
                {"field1":"v1","field2":"6","field3":"8999999",     "field4":"",        "field5":"26000",   "field6":"",        "field7":"60000",   "field8":"75000"},
                {"field1":"v1","field2":"6","field3":"9999999",     "field4":"",        "field5":"28000",   "field6":"",        "field7":"65000",   "field8":"80000"},
                {"field1":"v1","field2":"6","field3":"10999999",    "field4":"",        "field5":"30000",   "field6":"",        "field7":"70000",   "field8":"85000"},
                {"field1":"v1","field2":"6","field3":"11999999",    "field4":"",        "field5":"32000",   "field6":"",        "field7":"75000",   "field8":"90000"},
                {"field1":"v1","field2":"6","field3":"12999999",    "field4":"",        "field5":"34000",   "field6":"",        "field7":"80000",   "field8":"95000"},
                {"field1":"v1","field2":"6","field3":"13999999",    "field4":"",        "field5":"36000",   "field6":"",        "field7":"85000",   "field8":"100000"},
                {"field1":"v1","field2":"6","field3":"14999999",    "field4":"",        "field5":"38000",   "field6":"",        "field7":"90000",   "field8":"105000"},
                {"field1":"v1","field2":"6","field3":"15999999",    "field4":"",        "field5":"40000",   "field6":"",        "field7":"95000",   "field8":"110000"},
                {"field1":"v1","field2":"6","field3":"16999999",    "field4":"",        "field5":"42000",   "field6":"",        "field7":"100000",  "field8":"115000"},
                {"field1":"v1","field2":"6","field3":"17999999",    "field4":"",        "field5":"44000",   "field6":"",        "field7":"105000",  "field8":"120000"},
                {"field1":"v1","field2":"6","field3":"18999999",    "field4":"",        "field5":"46000",   "field6":"",        "field7":"110000",  "field8":"125000"},
                {"field1":"v1","field2":"6","field3":"20000000",    "field4":"",        "field5":"48000",   "field6":"",        "field7":"115000",  "field8":"130000"},
    
                {"field1":"v2","field2":"0","field3":"99999",       "field4":"15000",   "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v2","field2":"0","field3":"249999",      "field4":"15000",   "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v2","field2":"0","field3":"499999",      "field4":"15000",   "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v2","field2":"0","field3":"999999",      "field4":"",        "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v2","field2":"0","field3":"1999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"25000",   "field8":"40000"},
                {"field1":"v2","field2":"0","field3":"2999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"30000",   "field8":"45000"},
                {"field1":"v2","field2":"0","field3":"3999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"35000",   "field8":"50000"},
                {"field1":"v2","field2":"0","field3":"4999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"40000",   "field8":"55000"},
                {"field1":"v2","field2":"0","field3":"5999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"45000",   "field8":"60000"},
                {"field1":"v2","field2":"0","field3":"6999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"50000",   "field8":"65000"},
                {"field1":"v2","field2":"0","field3":"7999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"55000",   "field8":"70000"},
                {"field1":"v2","field2":"0","field3":"8999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"60000",   "field8":"75000"},
                {"field1":"v2","field2":"0","field3":"9999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"65000",   "field8":"80000"},
                {"field1":"v2","field2":"0","field3":"10999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"70000",   "field8":"85000"},
                {"field1":"v2","field2":"0","field3":"11999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"75000",   "field8":"90000"},
                {"field1":"v2","field2":"0","field3":"12999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"80000",   "field8":"95000"},
                {"field1":"v2","field2":"0","field3":"13999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"85000",   "field8":"100000"},
                {"field1":"v2","field2":"0","field3":"14999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"90000",   "field8":"105000"},
                {"field1":"v2","field2":"0","field3":"15999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"95000",   "field8":"110000"},
                {"field1":"v2","field2":"0","field3":"16999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"100000",  "field8":"115000"},
                {"field1":"v2","field2":"0","field3":"17999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"105000",  "field8":"120000"},
                {"field1":"v2","field2":"0","field3":"18999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"110000",  "field8":"125000"},
                {"field1":"v2","field2":"0","field3":"20000000",    "field4":"",        "field5":"",        "field6":"",        "field7":"115000",  "field8":"130000"},

                {"field1":"v2","field2":"15","field3":"99999",      "field4":"15000",   "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v2","field2":"15","field3":"249999",     "field4":"15000",   "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v2","field2":"15","field3":"499999",     "field4":"15000",   "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v2","field2":"15","field3":"999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v2","field2":"15","field3":"1999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"25000",   "field8":"40000"},
                {"field1":"v2","field2":"15","field3":"2999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"30000",   "field8":"45000"},
                {"field1":"v2","field2":"15","field3":"3999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"35000",   "field8":"50000"},
                {"field1":"v2","field2":"15","field3":"4999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"40000",   "field8":"55000"},
                {"field1":"v2","field2":"15","field3":"5999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"45000",   "field8":"60000"},
                {"field1":"v2","field2":"15","field3":"6999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"50000",   "field8":"65000"},
                {"field1":"v2","field2":"15","field3":"7999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"55000",   "field8":"70000"},
                {"field1":"v2","field2":"15","field3":"8999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"60000",   "field8":"75000"},
                {"field1":"v2","field2":"15","field3":"9999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"65000",   "field8":"80000"},
                {"field1":"v2","field2":"15","field3":"10999999",   "field4":"",        "field5":"",        "field6":"",        "field7":"70000",   "field8":"85000"},
                {"field1":"v2","field2":"15","field3":"11999999",   "field4":"",        "field5":"",        "field6":"",        "field7":"75000",   "field8":"90000"},
                {"field1":"v2","field2":"15","field3":"12999999",   "field4":"",        "field5":"",        "field6":"",        "field7":"80000",   "field8":"95000"},
                {"field1":"v2","field2":"15","field3":"13999999",   "field4":"",        "field5":"",        "field6":"",        "field7":"85000",   "field8":"100000"},
                {"field1":"v2","field2":"15","field3":"14999999",   "field4":"",        "field5":"",        "field6":"",        "field7":"90000",   "field8":"105000"},
                {"field1":"v2","field2":"15","field3":"15999999",   "field4":"",        "field5":"",        "field6":"",        "field7":"95000",   "field8":"110000"},
                {"field1":"v2","field2":"15","field3":"16999999",   "field4":"",        "field5":"",        "field6":"",        "field7":"100000",  "field8":"115000"},
                {"field1":"v2","field2":"15","field3":"17999999",   "field4":"",        "field5":"",        "field6":"",        "field7":"105000",  "field8":"120000"},
                {"field1":"v2","field2":"15","field3":"18999999",   "field4":"",        "field5":"",        "field6":"",        "field7":"110000",  "field8":"125000"},
                {"field1":"v2","field2":"15","field3":"20000000",   "field4":"",        "field5":"",        "field6":"",        "field7":"115000",  "field8":"130000"},

                {"field1":"v2","field2":"6","field3":"99999",       "field4":"15000",   "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v2","field2":"6","field3":"249999",      "field4":"15000",   "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v2","field2":"6","field3":"499999",      "field4":"15000",   "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v2","field2":"6","field3":"999999",      "field4":"",        "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v2","field2":"6","field3":"1999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"25000",   "field8":"40000"},
                {"field1":"v2","field2":"6","field3":"2999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"30000",   "field8":"45000"},
                {"field1":"v2","field2":"6","field3":"3999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"35000",   "field8":"50000"},
                {"field1":"v2","field2":"6","field3":"4999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"40000",   "field8":"55000"},
                {"field1":"v2","field2":"6","field3":"5999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"45000",   "field8":"60000"},
                {"field1":"v2","field2":"6","field3":"6999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"50000",   "field8":"65000"},
                {"field1":"v2","field2":"6","field3":"7999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"55000",   "field8":"70000"},
                {"field1":"v2","field2":"6","field3":"8999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"60000",   "field8":"75000"},
                {"field1":"v2","field2":"6","field3":"9999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"65000",   "field8":"80000"},
                {"field1":"v2","field2":"6","field3":"10999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"70000",   "field8":"85000"},
                {"field1":"v2","field2":"6","field3":"11999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"75000",   "field8":"90000"},
                {"field1":"v2","field2":"6","field3":"12999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"80000",   "field8":"95000"},
                {"field1":"v2","field2":"6","field3":"13999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"85000",   "field8":"100000"},
                {"field1":"v2","field2":"6","field3":"14999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"90000",   "field8":"105000"},
                {"field1":"v2","field2":"6","field3":"15999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"95000",   "field8":"110000"},
                {"field1":"v2","field2":"6","field3":"16999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"100000",  "field8":"115000"},
                {"field1":"v2","field2":"6","field3":"17999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"105000",  "field8":"120000"},
                {"field1":"v2","field2":"6","field3":"18999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"110000",  "field8":"125000"},
                {"field1":"v2","field2":"6","field3":"20000000",    "field4":"",        "field5":"",        "field6":"",        "field7":"115000",  "field8":"130000"},

                {"field1":"v3","field2":"0","field3":"99999",       "field4":"15000",   "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v3","field2":"0","field3":"249999",      "field4":"15000",   "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v3","field2":"0","field3":"499999",      "field4":"15000",   "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v3","field2":"0","field3":"999999",      "field4":"",        "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v3","field2":"0","field3":"1999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"25000",   "field8":"40000"},
                {"field1":"v3","field2":"0","field3":"2999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"30000",   "field8":"45000"},
                {"field1":"v3","field2":"0","field3":"3999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"35000",   "field8":"50000"},
                {"field1":"v3","field2":"0","field3":"4999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"40000",   "field8":"55000"},
                {"field1":"v3","field2":"0","field3":"5999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"45000",   "field8":"60000"},
                {"field1":"v3","field2":"0","field3":"6999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"50000",   "field8":"65000"},
                {"field1":"v3","field2":"0","field3":"7999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"55000",   "field8":"70000"},
                {"field1":"v3","field2":"0","field3":"8999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"60000",   "field8":"75000"},
                {"field1":"v3","field2":"0","field3":"9999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"65000",   "field8":"80000"},
                {"field1":"v3","field2":"0","field3":"10999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"70000",   "field8":"85000"},
                {"field1":"v3","field2":"0","field3":"11999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"75000",   "field8":"90000"},
                {"field1":"v3","field2":"0","field3":"12999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"80000",   "field8":"95000"},
                {"field1":"v3","field2":"0","field3":"13999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"85000",   "field8":"100000"},
                {"field1":"v3","field2":"0","field3":"14999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"90000",   "field8":"105000"},
                {"field1":"v3","field2":"0","field3":"15999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"95000",   "field8":"110000"},
                {"field1":"v3","field2":"0","field3":"16999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"100000",  "field8":"115000"},
                {"field1":"v3","field2":"0","field3":"17999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"105000",  "field8":"120000"},
                {"field1":"v3","field2":"0","field3":"18999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"110000",  "field8":"125000"},
                {"field1":"v3","field2":"0","field3":"20000000",    "field4":"",        "field5":"",        "field6":"",        "field7":"115000",  "field8":"130000"},

                {"field1":"v3","field2":"15","field3":"99999",      "field4":"15000",   "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v3","field2":"15","field3":"249999",     "field4":"15000",   "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v3","field2":"15","field3":"499999",     "field4":"15000",   "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v3","field2":"15","field3":"999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v3","field2":"15","field3":"1999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"25000",   "field8":"40000"},
                {"field1":"v3","field2":"15","field3":"2999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"30000",   "field8":"45000"},
                {"field1":"v3","field2":"15","field3":"3999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"35000",   "field8":"50000"},
                {"field1":"v3","field2":"15","field3":"4999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"40000",   "field8":"55000"},
                {"field1":"v3","field2":"15","field3":"5999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"45000",   "field8":"60000"},
                {"field1":"v3","field2":"15","field3":"6999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"50000",   "field8":"65000"},
                {"field1":"v3","field2":"15","field3":"7999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"55000",   "field8":"70000"},
                {"field1":"v3","field2":"15","field3":"8999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"60000",   "field8":"75000"},
                {"field1":"v3","field2":"15","field3":"9999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"65000",   "field8":"80000"},
                {"field1":"v3","field2":"15","field3":"10999999",   "field4":"",        "field5":"",        "field6":"",        "field7":"70000",   "field8":"85000"},
                {"field1":"v3","field2":"15","field3":"11999999",   "field4":"",        "field5":"",        "field6":"",        "field7":"75000",   "field8":"90000"},
                {"field1":"v3","field2":"15","field3":"12999999",   "field4":"",        "field5":"",        "field6":"",        "field7":"80000",   "field8":"95000"},
                {"field1":"v3","field2":"15","field3":"13999999",   "field4":"",        "field5":"",        "field6":"",        "field7":"85000",   "field8":"100000"},
                {"field1":"v3","field2":"15","field3":"14999999",   "field4":"",        "field5":"",        "field6":"",        "field7":"90000",   "field8":"105000"},
                {"field1":"v3","field2":"15","field3":"15999999",   "field4":"",        "field5":"",        "field6":"",        "field7":"95000",   "field8":"110000"},
                {"field1":"v3","field2":"15","field3":"16999999",   "field4":"",        "field5":"",        "field6":"",        "field7":"100000",  "field8":"115000"},
                {"field1":"v3","field2":"15","field3":"17999999",   "field4":"",        "field5":"",        "field6":"",        "field7":"105000",  "field8":"120000"},
                {"field1":"v3","field2":"15","field3":"18999999",   "field4":"",        "field5":"",        "field6":"",        "field7":"110000",  "field8":"125000"},
                {"field1":"v3","field2":"15","field3":"20000000",   "field4":"",        "field5":"",        "field6":"",        "field7":"115000",  "field8":"130000"},

                {"field1":"v3","field2":"6","field3":"99999",       "field4":"15000",   "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v3","field2":"6","field3":"249999",      "field4":"15000",   "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v3","field2":"6","field3":"499999",      "field4":"15000",   "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v3","field2":"6","field3":"999999",      "field4":"",        "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v3","field2":"6","field3":"1999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"25000",   "field8":"40000"},
                {"field1":"v3","field2":"6","field3":"2999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"30000",   "field8":"45000"},
                {"field1":"v3","field2":"6","field3":"3999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"35000",   "field8":"50000"},
                {"field1":"v3","field2":"6","field3":"4999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"40000",   "field8":"55000"},
                {"field1":"v3","field2":"6","field3":"5999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"45000",   "field8":"60000"},
                {"field1":"v3","field2":"6","field3":"6999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"50000",   "field8":"65000"},
                {"field1":"v3","field2":"6","field3":"7999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"55000",   "field8":"70000"},
                {"field1":"v3","field2":"6","field3":"8999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"60000",   "field8":"75000"},
                {"field1":"v3","field2":"6","field3":"9999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"65000",   "field8":"80000"},
                {"field1":"v3","field2":"6","field3":"10999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"70000",   "field8":"85000"},
                {"field1":"v3","field2":"6","field3":"11999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"75000",   "field8":"90000"},
                {"field1":"v3","field2":"6","field3":"12999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"80000",   "field8":"95000"},
                {"field1":"v3","field2":"6","field3":"13999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"85000",   "field8":"100000"},
                {"field1":"v3","field2":"6","field3":"14999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"90000",   "field8":"105000"},
                {"field1":"v3","field2":"6","field3":"15999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"95000",   "field8":"110000"},
                {"field1":"v3","field2":"6","field3":"16999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"100000",  "field8":"115000"},
                {"field1":"v3","field2":"6","field3":"17999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"105000",  "field8":"120000"},
                {"field1":"v3","field2":"6","field3":"18999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"110000",  "field8":"125000"},
                {"field1":"v3","field2":"6","field3":"20000000",    "field4":"",        "field5":"",        "field6":"",        "field7":"115000",  "field8":"130000"},
        
                {"field1":"v4","field2":"0","field3":"99999",       "field4":"",        "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v4","field2":"0","field3":"249999",      "field4":"",        "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v4","field2":"0","field3":"499999",      "field4":"",        "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v4","field2":"0","field3":"999999",      "field4":"",        "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v4","field2":"0","field3":"1999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"25000",   "field8":"40000"},
                {"field1":"v4","field2":"0","field3":"2999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"30000",   "field8":"45000"},
                {"field1":"v4","field2":"0","field3":"3999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"35000",   "field8":"50000"},
                {"field1":"v4","field2":"0","field3":"4999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"40000",   "field8":"55000"},
                {"field1":"v4","field2":"0","field3":"5999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"45000",   "field8":"60000"},
                {"field1":"v4","field2":"0","field3":"6999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"50000",   "field8":"65000"},
                {"field1":"v4","field2":"0","field3":"7999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"55000",   "field8":"70000"},
                {"field1":"v4","field2":"0","field3":"8999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"60000",   "field8":"75000"},
                {"field1":"v4","field2":"0","field3":"9999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"65000",   "field8":"80000"},
                {"field1":"v4","field2":"0","field3":"10999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"70000",   "field8":"85000"},
                {"field1":"v4","field2":"0","field3":"11999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"75000",   "field8":"90000"},
                {"field1":"v4","field2":"0","field3":"12999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"80000",   "field8":"95000"},
                {"field1":"v4","field2":"0","field3":"13999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"85000",   "field8":"100000"},
                {"field1":"v4","field2":"0","field3":"14999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"90000",   "field8":"105000"},
                {"field1":"v4","field2":"0","field3":"15999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"95000",   "field8":"110000"},
                {"field1":"v4","field2":"0","field3":"16999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"100000",  "field8":"115000"},
                {"field1":"v4","field2":"0","field3":"17999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"105000",  "field8":"120000"},
                {"field1":"v4","field2":"0","field3":"18999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"110000",  "field8":"125000"},
                {"field1":"v4","field2":"0","field3":"20000000",    "field4":"",        "field5":"",        "field6":"",        "field7":"115000",  "field8":"130000"},
                        
                {"field1":"v4","field2":"15","field3":"99999",      "field4":"",        "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v4","field2":"15","field3":"249999",     "field4":"",        "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v4","field2":"15","field3":"499999",     "field4":"",        "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v4","field2":"15","field3":"999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v4","field2":"15","field3":"1999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"25000",   "field8":"40000"},
                {"field1":"v4","field2":"15","field3":"2999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"30000",   "field8":"45000"},
                {"field1":"v4","field2":"15","field3":"3999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"35000",   "field8":"50000"},
                {"field1":"v4","field2":"15","field3":"4999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"40000",   "field8":"55000"},
                {"field1":"v4","field2":"15","field3":"5999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"45000",   "field8":"60000"},
                {"field1":"v4","field2":"15","field3":"6999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"50000",   "field8":"65000"},
                {"field1":"v4","field2":"15","field3":"7999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"55000",   "field8":"70000"},
                {"field1":"v4","field2":"15","field3":"8999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"60000",   "field8":"75000"},
                {"field1":"v4","field2":"15","field3":"9999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"65000",   "field8":"80000"},
                {"field1":"v4","field2":"15","field3":"10999999",   "field4":"",        "field5":"",        "field6":"",        "field7":"70000",   "field8":"85000"},
                {"field1":"v4","field2":"15","field3":"11999999",   "field4":"",        "field5":"",        "field6":"",        "field7":"75000",   "field8":"90000"},
                {"field1":"v4","field2":"15","field3":"12999999",   "field4":"",        "field5":"",        "field6":"",        "field7":"80000",   "field8":"95000"},
                {"field1":"v4","field2":"15","field3":"13999999",   "field4":"",        "field5":"",        "field6":"",        "field7":"85000",   "field8":"100000"},
                {"field1":"v4","field2":"15","field3":"14999999",   "field4":"",        "field5":"",        "field6":"",        "field7":"90000",   "field8":"105000"},
                {"field1":"v4","field2":"15","field3":"15999999",   "field4":"",        "field5":"",        "field6":"",        "field7":"95000",   "field8":"110000"},
                {"field1":"v4","field2":"15","field3":"16999999",   "field4":"",        "field5":"",        "field6":"",        "field7":"100000",  "field8":"115000"},
                {"field1":"v4","field2":"15","field3":"17999999",   "field4":"",        "field5":"",        "field6":"",        "field7":"105000",  "field8":"120000"},
                {"field1":"v4","field2":"15","field3":"18999999",   "field4":"",        "field5":"",        "field6":"",        "field7":"110000",  "field8":"125000"},
                {"field1":"v4","field2":"15","field3":"20000000",   "field4":"",        "field5":"",        "field6":"",        "field7":"115000",  "field8":"130000"},

                {"field1":"v4","field2":"6","field3":"99999",       "field4":"",        "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v4","field2":"6","field3":"249999",      "field4":"",        "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v4","field2":"6","field3":"499999",      "field4":"",        "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v4","field2":"6","field3":"999999",      "field4":"",        "field5":"",        "field6":"",        "field7":"20000",   "field8":"35000"},
                {"field1":"v4","field2":"6","field3":"1999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"25000",   "field8":"40000"},
                {"field1":"v4","field2":"6","field3":"2999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"30000",   "field8":"45000"},
                {"field1":"v4","field2":"6","field3":"3999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"35000",   "field8":"50000"},
                {"field1":"v4","field2":"6","field3":"4999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"40000",   "field8":"55000"},
                {"field1":"v4","field2":"6","field3":"5999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"45000",   "field8":"60000"},
                {"field1":"v4","field2":"6","field3":"6999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"50000",   "field8":"65000"},
                {"field1":"v4","field2":"6","field3":"7999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"55000",   "field8":"70000"},
                {"field1":"v4","field2":"6","field3":"8999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"60000",   "field8":"75000"},
                {"field1":"v4","field2":"6","field3":"9999999",     "field4":"",        "field5":"",        "field6":"",        "field7":"65000",   "field8":"80000"},
                {"field1":"v4","field2":"6","field3":"10999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"70000",   "field8":"85000"},
                {"field1":"v4","field2":"6","field3":"11999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"75000",   "field8":"90000"},
                {"field1":"v4","field2":"6","field3":"12999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"80000",   "field8":"95000"},
                {"field1":"v4","field2":"6","field3":"13999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"85000",   "field8":"100000"},
                {"field1":"v4","field2":"6","field3":"14999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"90000",   "field8":"105000"},
                {"field1":"v4","field2":"6","field3":"15999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"95000",   "field8":"110000"},
                {"field1":"v4","field2":"6","field3":"16999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"100000",  "field8":"115000"},
                {"field1":"v4","field2":"6","field3":"17999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"105000",  "field8":"120000"},
                {"field1":"v4","field2":"6","field3":"18999999",    "field4":"",        "field5":"",        "field6":"",        "field7":"110000",  "field8":"125000"},
                {"field1":"v4","field2":"6","field3":"20000000",    "field4":"",        "field5":"",        "field6":"",        "field7":"115000",  "field8":"130000"}
            ];          
    $(".input__radio input[name=nalog]").change(function(e){
        ratePrice();
    });
    $(".sort select").change(function(e){
        ratePrice();
    });
    $( function() {
        $( "#slider" ).slider({
            max: 20000000,
            min: 0,
            orientation: "horizontal",
            range: "min",
            step: 10000,
            animate: true, 
            slide: function( event, ui ) {
                $(".calc__value__slider").html( $("#slider").slider('value') + " ₽" );
                ratePrice();
            }
        });
      } );


	$.post( "test.php", $( "form.calc" ).serialize(), function (data) {
		// ответ от сервера
	}, "json" );


	$("input[type=radio]").click(function(){
		//console.log( $(this).val() );
	});


	/* END  slider */

	/* анимация блоков */
	new WOW().init({
		mobile: false
	});


	/* START preloader*/
	(function () {
		if ( Date.now() - preloader.timerStart >= preloader.duration  ) {
			$("#preloader-cube").animate({"opacity" : "0"}, preloader.duration / 2, function () {
				$("#preloader-cube").css("display", "none");
			});
		} else {
			setTimeout(function () {
				$("#preloader-cube").animate({"opacity" : "0"}, preloader.duration / 2, function () {
					$("#preloader-cube").css("display", "none");
				});
			}, preloader.duration / 1.7)
		}
	})();
	/* END preloader */

	/* START Открытие меню */
	$(".btn__menu").on("click", function () {
		$(this).toggleClass("active");

		if ( $(this).hasClass("active") ) {
			$(".navigation__content").addClass("active");
		} else {
			$(".navigation__content").removeClass("active");
		}
	});

	$(".btn_header").on("click", function () {
		if( $(".navigation__content").hasClass("active") ) {
			$(".navigation__content").removeClass("active");
			$(".btn__menu").removeClass("active");
			$("#burger").removeClass("active");
		}
	});
	/* END откртие меню*/

	/* START красиво для hover в navigation */
    function activeMenu () {
        var ael = $(".navigation li.active a");
        var width = ael.width();
        var elem = $(".navigation li.active");
        var offset = elem.offset().left - elem.parent().offset().left;

        offset += parseInt( ael.css("padding-left") );
        $(".navigation__line").width( width );
        $(".navigation__line").css("left", offset);


        $(".navigation__line").css("bottom", -6);
    }
    activeMenu();

    $(".navigation ul li a").hover(function () {
        var width = $(this).width();

        var elem = $(this);
        var offset = elem.offset().left - elem.parent().parent().offset().left;
        offset += parseInt(elem.css("padding-left"));

        $(".navigation__line").width(width);
        $(".navigation__line").css("left", offset);
        $(".navigation__line").css("bottom", "-6px");
    }, function () {
        // Забрать красоту! ВСЮ!
        // $(".navigation__line").css("bottom", "-500%");
        activeMenu();
    });


    /* END красиво для hover в navigation */

	if ( $(window).scrollTop() > 0 ) {
        //$("header").addClass("bg-dark");
    }
	$(window).on("scroll", function () {
		if ( $(window).scrollTop() > 0 ) {
			//$("header").addClass("bg-dark");
		}  else {
			//$("header").removeClass("bg-dark");
		}

		/* START кнопка вверх */
		if ( $(window).scrollTop() > 100 ) {
			$(".btn-to-top").addClass("btn-to-top_active");
		} else {
			$(".btn-to-top").removeClass("btn-to-top_active");
		}

	});
	$('.btn-to-top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 400);
        return false;
    });
	/* END кнопка вверх */










	$('#modalCalc').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) ;
		var recipient = button.data('whatever') ;
		//var modal = $(this);
		//modal.find('.modal-body .calc__val').val(recipient);
	})
	/* END calc event */



	// Плавный переход для якорей
	$('a[href^="#"]').on('click', function(event) {
	    // отменяем стандартное действие
	    event.preventDefault();
	    
	    var sc = $(this).attr("href"),
	        dn = $(sc).offset().top;
	        dn -= 90;
	    /*
	    * sc - в переменную заносим информацию о том, к какому блоку надо перейти
	    * dn - определяем положение блока на странице
	    */
	    
	    $('html, body').animate({scrollTop: dn}, 1000);
	    
	    /*
	    * 1000 скорость перехода в миллисекундах
	    */
	  });












	// SLIDER
	$('.slider_trust').slick({
        dots: true,
        infinite: false,
        speed: 300,
        slidesToShow: 5,
        adaptiveHeight: true,
        lazyLoad: 'ondemand',
        slidesToScroll: 2,
        arrows: false,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 981,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });


    $('.slider_perhaps').slick({
        dots: false,
        infinite: false,
        speed: 300,
        slidesToShow: 1,
        adaptiveHeight: true,
        lazyLoad: 'ondemand',
        slidesToScroll: 1,
         nextArrow: '<div class="sldier_arrow_next"><svg class="card_item_arrow" width="50" height="50" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M33.1563 43.4063L30.9063 45.6562L11.375 26.125L10.3032 25L11.3766 23.875L30.9078 4.34375L33.1578 6.59375L14.7438 25L33.1563 43.4063Z" fill="#154880"/></svg></div>',
        prevArrow: '<div class="sldier_arrow_prev"><svg class="card_item_arrow" width="50" height="50" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M33.1563 43.4063L30.9063 45.6562L11.375 26.125L10.3032 25L11.3766 23.875L30.9078 4.34375L33.1578 6.59375L14.7438 25L33.1563 43.4063Z" fill="#154880"/></svg></div>',
    });







    
    (function() {
        var x, i, j, selElmnt, a, b, c, wrapperHeader ;
        /* Look for any elements with the class "custom__select": */
        x = document.getElementsByClassName("custom__select");



        for (i = 0; i < x.length; i++) {
            selElmnt = x[i].getElementsByTagName("select")[0];
            /* For each element, create a new DIV that will act as the selected item: */
            a = document.createElement("DIV");
            a.setAttribute("class", "select-selected");

            wrapperHeader = document.createElement("DIV");
            wrapperHeader.setAttribute("class", "text__underline");
            wrapperHeader.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
            a.appendChild (wrapperHeader);

            x[i].appendChild(a);

            /* For each element, create a new DIV that will contain the option list: */
            b = document.createElement("DIV");
            b.setAttribute("class", "select-items select-hide");
            for (j = 1; j < selElmnt.length; j++) {
                /* For each option in the original select element,
                create a new DIV that will act as an option item: */
                c = document.createElement("DIV");
                c.innerHTML = selElmnt.options[j].innerHTML;
                c.addEventListener("click", function(e) {
                    /* When an item is clicked, update the original select box,
                    and the selected item: */
                    var y, i, k, s, h;
                    s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                    h = this.parentNode.previousSibling;

                    for (i = 0; i < s.length; i++) {
                        if (s.options[i].innerHTML == this.innerHTML) {
                            s.selectedIndex = i;
                            //h.innerHTML = this.innerHTML;
                            $(this).parent(".select-items").parent(".custom__select").find(".text__underline").html(this.innerHTML);
                            

                            y = this.parentNode.getElementsByClassName("same-as-selected");
                            for (k = 0; k < y.length; k++) {
                                y[k].removeAttribute("class");
                            }
                            this.setAttribute("class", "same-as-selected");
                            break;
                        }
                    }
                    h.click();

                    if ( !( $(this).parent(".select-items").parent(".custom__select").find("select").val() == 0 ) ) {
                        $(this).parent(".select-items").parent(".custom__select").find(".select-selected").addClass("select-selected__active");
                    }
                });
                b.appendChild(c);
            }
            x[i].appendChild(b);
            a.addEventListener("click", function(e) {
                /* When the select box is clicked, close any other select boxes,
                and open/close the current select box: */
                e.stopPropagation();
                closeAllSelect(this);
                this.nextSibling.classList.toggle("select-hide");
                this.classList.toggle("select-arrow-active");

            });

            

            $(".select-selected").on('click', function () {

                if ( $(this).parent().hasClass("active") ) {
                    $(this).parent().removeClass("active");
                    $(".custom__select").removeClass("active");
                } else {
                    $(".custom__select").removeClass("active");
                    $(this).parent().addClass("active");
                }

                checkedCalc($(this));
            });
                

        }



        function closeAllSelect(elmnt) {
            /* A function that will close all select boxes in the document,
            except the current select box: */
            var x, y, i, arrNo = [];
            x = document.getElementsByClassName("select-items");
            y = document.getElementsByClassName("select-selected");
            for (i = 0; i < y.length; i++) {
                if (elmnt == y[i]) {
                    arrNo.push(i)
                } else {
                    y[i].classList.remove("select-arrow-active");
                }
            }
            for (i = 0; i < x.length; i++) {
                if (arrNo.indexOf(i)) {
                    x[i].classList.add("select-hide");
                }
            }




        }

        /* If the user clicks anywhere outside the select box,
        then close all select boxes: */
        document.addEventListener("click", closeAllSelect);
    })();



    $(".tik__hover").hover(function (e) {
        $(this).parent().find(".tik__content").css("display", "block");
        var out = $(this).parent().parent().width();
        var width = $(".tik .container").outerWidth();

        width -= out;

        $(this).parent().find(".tik__content").width(width);



        var f = $(this).offset().top - $(window).scrollTop();
        var left = $(".tik__hover").offset().left;

        $(this).parent().find(".tik__content").css("top", (f + 40) );
        $(this).parent().find(".tik__content").css("left", left);

    }, function (e) {
        $(this).parent().find(".tik__content").css("display", "none");
    });



    // ЕБУЧИЙ КАЛЬКУЛЯТОР!
    $(".custom__select select").change(function () {
        console.log( $(this) );
        //console.log( $(this).val() );
    });

    function checkedCalc(that) {

        var arr = [];

        console.log( $(that).parent().find("select").val() );


    }
});


